function init_map(args){
    var data_location = "http://"+document.location.hostname+"/media/chi_map/data/";
    var chicago = new L.LatLng(41.87, -87.62);
    var tilesetUrl =  'http://{s}.acetate.geoiq.com/tiles/acetate/{z}/{x}/{y}.png';
    var tilesetAttrib = '2011 GeoIQ &#038; Stamen, Data from OSM and Natural Earth';

    // create the tile layer with correct attribution
    var osm = new L.TileLayer(tilesetUrl, {
        minZoom: 2,
        maxZoom: 20,
        attribution: tilesetAttrib
    }); 

    // set up the map
    var map = new L.Map('map',{
        center:chicago,
        zoom: 11,
        layers: [osm]    
    });

    //Trinity Church
    var altIcon  = L.Icon.extend({
        iconUrl: data_location+'Icons/marker-alt.png',
    });

    var trinity = new L.Marker(new L.LatLng(41.845463,-87.622563), {icon:new altIcon()}).bindPopup("<h2>Camping</h2>The pastor of Trinity Episcopal Anglican Church at 26th and Michigan will allow protesters to set up camp during the summit");
    map.addLayer(trinity);

    //United Church of Christ
    var altIcon  = L.Icon.extend({
        iconUrl: data_location+'Icons/marker-alt.png',
    });

    var ChurchofChrist = new L.Marker(new L.LatLng(41.936422,-87.645111), {icon:new altIcon()}).bindPopup("<h2>Main Convergance Space</h2>The Wellington Avenue United Church of Christ - 615 W. Wellington <a href=\"/article/main-convergence-center/\">Read More</a>");
    map.addLayer(ChurchofChrist);

   //wellness center
    var altIcon  = L.Icon.extend({
        iconUrl: data_location+'Icons/marker-alt.png',
    });

    var ChurchofChrist = new L.Marker(new L.LatLng(41.87348,-87.62901), {icon:new altIcon()}).bindPopup("<h2>Wellness Center</h2>During NATO Protests the Wellness Center will offer first aid, chemical decontamination, health consultation, herbal remedies, bodywork, massage, and more. <a href=\"http://natoprotest.org/article/health-and-wellness-care-resources/\">Read More</a>");
    map.addLayer(ChurchofChrist);
   
 
    //security zone
    var p1 = new L.LatLng(41.854262,-87.622311),
        p2 = new L.LatLng(41.847389,-87.622225),
        p3 = new L.LatLng(41.847197,-87.604973),
        p4 = new L.LatLng(41.853623,-87.605016),
        polylinePoints = [p1, p2, p3, p4];

    // create the rectangle
    var rectangle = new L.Polygon(polylinePoints, {
        fillColor: "#ff7800",
        color: "#000000",
        opacity: 1,
        weight: 2
    }).bindPopup("The Security Perimeter");
     // add the rectangle to the map
    map.addLayer(rectangle);
    
     //add cermak
        p1 = new L.LatLng(41.853611,-87.640976),
        p2 = new L.LatLng(41.853455,-87.640404),
        p3 = new L.LatLng(41.852825,-87.640884),
        p4 = new L.LatLng(41.852837,-87.641434),
        p5 = new L.LatLng(41.853611,-87.640976),
        polylinePoints = [p1, p2, p3, p4,p5];

    var cermak = new L.Polygon(polylinePoints, {
        fillColor: "#ff7800",
        color: "#000000",
        opacity: 1,
        weight: 2
    }).bindPopup("500 W Cermak");

     // add the rectangle to the map
    map.addLayer(cermak);

    //add the red zone
    p1 = new L.LatLng(41.879372,-87.635254),
    p2 = new L.LatLng(41.879482,-87.627686),
    p3 = new L.LatLng(41.874561,-87.627617),
    p4 = new L.LatLng(41.874466,-87.635040),
    p5 = new L.LatLng(41.879372,-87.635254),
    polylinePoints = [p1, p2, p3, p4,p5];

    // create the rectangle
    var redzone = new L.Polygon(polylinePoints, {
        fillColor: "#FF0000",
        color: "#000000",
        opacity: 1,
        weight: 2
    }).bindPopup("<b>The Red Zone</b><br><a href=\"http://natoprotest.org/article/operation-red-zone/\">A \"red zone\" will surround federal buildings</a><br>manned by \"traveling swarms\" of armed federal agents. The zone will only be restricted in the event of \"civil disobedience.\"");
     // add the rectangle to the map
    map.addLayer(redzone);

    //add events
    var eventJSON = new L.GeoJSON(null,{
            pointToLayer: function (latlng) {
                return new  L.Marker(latlng);
            }
        });

    eventJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>"
            +e.properties.Name+"</h1><h2>"
            +e.properties.Start+"<br>"
            +e.properties.Description
            +" |&nbsp;<a href='"
            +e.properties.Url+"'>View&nbsp;More</a></h2>");
        }
    });
    

    eventJSON.addGeoJSON(args.event_json);

   // if(args.event){
        map.addLayer(eventJSON); 
   // }
    
    var p1 = new L.LatLng(41.878285,-87.62058),
        p2 = new L.LatLng(41.878221,-87.627747),
        p3 = new L.LatLng(41.874394,-87.627581),
        p4 = new L.LatLng(41.87445,-87.62433),
        p5 = new L.LatLng(41.852765,-87.623788),
        polylinePoints = [p1, p2, p3, p4, p5],
        polyline = new L.Polyline(polylinePoints);

    polyline.bindPopup("<h2>CANG8 March</h2> Petrillo Band Shell |<a href=\"http://natoprotest.org/article/cang8-march/\">View More</a>");
    map.addLayer(polyline);

    //add nurses march

     var p1 = new L.LatLng(41.889515,-87.620277),
        p2 = new L.LatLng(41.889595,-87.621590),
        p3 = new L.LatLng(41.890984,-87.621696),
        p4 = new L.LatLng(41.890923,-87.624184),
        p5 = new L.LatLng(41.888222,-87.624420),
        p6 = new L.LatLng(41.880856,-87.624313),
        p7 = new L.LatLng(41.880440,-87.620728),
        p8 = new L.LatLng(41.878429,-87.620644),
        polylinePoints = [p1, p2, p3, p4, p5, p6,p7,p8],
        polyline2 = new L.Polyline(polylinePoints);
   
    polyline2.bindPopup("<h2>National Nurses United March</h2>Sheraton Chicago Hotel & Towers |<a href=\"http://natoprotest.org/article/robin-hood-will-search-city-world-leaders/\">View More</a>");
    map.addLayer(polyline2);

    //setup Icons
    var ImageIcon = L.Icon.extend({
        iconUrl: data_location+'Icons/police.png',
        shadowUrl: null,
        iconSize: new L.Point(70, 70),
        iconAnchor: new L.Point(35, 70),
        popupAnchor: new L.Point(0, -45)
    });

    //add donors
    var DonorsLayer = new L.GeoJSON(null, {
        pointToLayer: function (latlng) {
                return new L.Marker(latlng, {icon: new ImageIcon()});
                //return new L.Circle(latlng, 40, StationMarkerOptions);
        }            
    });
    
    DonorsLayer.on("featureparse", function (e) {
        e.layer.options.icon.iconUrl = gen_icon(e.properties.industry);
        if (e.properties && e.properties.description){
            e.layer.bindPopup("<h2>"+e.properties.name+"</h2>"+ e.properties.address+"<br>"+e.properties.description);
        }
    });  
    
    $.ajax({
       url: data_location+"donors.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        DonorsLayer.addGeoJSON(data);
    });
    
    //add World Bussness
    var WorldbussnessChiLayer = new L.GeoJSON(null, {
         pointToLayer: function (latlng) {
            return new L.Marker(latlng, {icon: new ImageIcon()});
        }             
    });   
    
    WorldbussnessChiLayer.on("featureparse", function (e) {
        e.layer.options.icon.iconUrl = gen_icon(e.properties.industry);
        if (e.properties && e.properties.description){
            e.layer.bindPopup("<h2>"+e.properties.name+"</h2>"+e.properties.address+"<br>"+e.properties.description);
        }
    });     

    $.ajax({
       url: data_location+"worldbussnessChi.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        WorldbussnessChiLayer.addGeoJSON(data);
    });
    
    /*
     * Banks
     */ 
     
    //Chase
    
    //marker settings
    var ChaseMarkerOptions = {
        fillColor: "#ff7800",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };
        
    //set up geojson layer
    var ChaseBankLayer = new L.LayerGroup();
    var ChaseBankJSON = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, ChaseMarkerOptions);
        }            
    }); 
    var ChaseHQ = new L.Marker(new L.LatLng(41.881793,-87.63008), {icon:new altIcon()}).bindPopup("<b>Headquarters:</b> Chase Tower, 10 South Dearborn Street");
    ChaseBankLayer.addLayer(ChaseHQ).addLayer(ChaseBankJSON);
    //set up popups
    ChaseBankJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>Chase ATM or Branch</h1>"+e.properties.Description);
        }
        
    });  
    //Chase bank data
    $.ajax({
       url: data_location+"chase.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        ChaseBankJSON.addGeoJSON(data);
    });
   
   //BOA
    var BoALayer =  new L.LayerGroup();
    var BoAJSON = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, ChaseMarkerOptions);
        }            
    });   
    var BoAHQ = new L.Marker(new L.LatLng(41.879908,-87.632161), {icon:new altIcon()}).bindPopup("<b>Headquarters:</b> Bank of America Building, 135 South LaSalle ");
    BoALayer.addLayer(BoAJSON).addLayer(BoAHQ);
    
    BoAJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>Bank Of A. ATM or Branch</h1>"+e.properties.Description);
        }
    });  
    
    $.ajax({
       url: data_location+"BoA.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        BoAJSON.addGeoJSON(data);
    });
    
    //citi   
    var CitiLayer =  new L.LayerGroup();
    var CitiJSON = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, ChaseMarkerOptions);
        }            
    });   
    
    var CitiHQ = new L.Marker(new L.LatLng(41.882138,-87.640405), {icon:new altIcon()}).bindPopup("<b>Headquarters:</b> Citigroup Center, 500 West Madison");
    CitiLayer.addLayer(CitiJSON).addLayer(CitiHQ);    
    CitiJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>Citi ATM or Branch</h1>"+e.properties.Description+"<a>");
        }
    });  
     
    $.ajax({
       url: data_location+"citi.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        CitiJSON.addGeoJSON(data);
    });
    
    //police stations
    //create custom Icons
    var policeIcon = new ImageIcon(); 
    var StationLayer = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Marker(latlng, {icon: policeIcon});
                //return new L.Circle(latlng, 40, StationMarkerOptions);
        }            
    }); 
    StationLayer.on("featureparse", function (e) {
        if (e.properties && e.properties.description){
            e.layer.bindPopup("<b>Station:"+e.properties.name+"</b><br>"+e.properties.address+"<br><a href=\""+e.properties.description+"\">More Info</a>");
        }
    }); 
     
    $.ajax({
       url: data_location+"stations.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        StationLayer.addGeoJSON(data);
    });

    //Hospitals
    var HospitalMarkerOptions = {
        fillColor: "White",
        color: "Red",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };   
    var HospitalLayer = new L.GeoJSON(null, {
        pointToLayer: function (latlng) {
            return new L.Circle(latlng, 40, HospitalMarkerOptions);
        }               
    }); 

    HospitalLayer.on("featureparse", function (e) {
        if (e.properties){
            e.layer.bindPopup("<b>"+e.properties.name+"</b>");
        }
    });  
    
    $.ajax({
       url: data_location+"hospitals.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        HospitalLayer.addGeoJSON(data);
    });    
        
    //CCTV 
    var CCTVMarkerOptions = {
        fillColor: "black",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };

    var CCTVLayer = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, CCTVMarkerOptions);
        }            
    });  
    
    CCTVLayer.on("featureparse", function (e) {
        if (e.properties ){
            e.layer.bindPopup(e.properties.Description);
        }
    }); 
    //CCTV  data
    $.ajax({
       url: data_location+"cctv.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        CCTVLayer.addGeoJSON(data);
    });
     
    //Homes

    var HouseMarkerOptions = {
        fillColor: "#20B727",
        color: "#5454EF",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };
    
    var HouseLayer = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Circle(latlng, 40, HouseMarkerOptions);
        }            
    }); 
    
    HouseLayer.on("featureparse", function (e) {
        if (e.properties){
            e.layer.bindPopup("<h2>"+e.properties.name+"</h2><b>Entry:</b>"+e.properties.description+"<br>"+e.properties.address);
        }
    });    
    
    $.ajax({
       url: data_location+"build.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        HouseLayer.addGeoJSON(data);
    });

    //clinics
    //clinics

    var clinicsIcon  = L.Icon.extend({
        iconUrl: data_location+'Icons/green_MarkerO.png',
    });

    var clinicLayer = new L.GeoJSON(null, {
            pointToLayer: function (latlng) {
                return new L.Marker(latlng, {icon:new clinicsIcon()});
        }            
    }); 
    clinicLayer.on("featureparse", function (e) {
        if (e.properties && e.properties.description){
            e.layer.bindPopup("<b>"+e.properties.name+"</b><br>"+e.properties.description+"<br><a href=\"http://natoprotest.org/article/mental-health-movement-calls-nato-protesters-occup/\">More Info</a>");
        }
    }); 
     
    $.ajax({
       url: data_location+"clinics.json",
       dataType:'json',
    }).done(function(data) { 
        //create geojson layer
        clinicLayer.addGeoJSON(data);
    });



    //setup controlls
    var layersControl = new L.Control.Layers(null, {
            "Events":eventJSON,
            "Summit Sponsors": DonorsLayer,
            "World Business Chicago":WorldbussnessChiLayer,
            "Chase Banks": ChaseBankLayer,
            "Bank Of America":BoALayer,
            "Citi": CitiLayer,
            "Closed Circuit Cameras": CCTVLayer,
	    "Occupied Clinics": clinicLayer,
            "Abandoned Homes": HouseLayer,
            "Police Stations":StationLayer,
            "Hospitals":HospitalLayer
        },{'collapsed':false});

    map.addControl(layersControl);

    if(args.clinics){
        map.addLayer(clinicLayer); 
    }

    //add location
    map.locateAndSetView(12);
    map.on('locationfound', onLocationFound);

    function onLocationFound(e) {
        var radius = e.accuracy / 2;

        var marker = new L.Marker(e.latlng);
        map.addLayer(marker);
        marker.bindPopup("You are within " + radius + " meters from this point").openPopup();

        var circle = new L.Circle(e.latlng, radius);
        map.addLayer(circle);
    }
    
    map.on('locationerror', onLocationError);

    function onLocationError(e) {
        alert(e.message);
    } 
    
    function gen_icon(industry){
        if(industry == "food"){
            return data_location+'Icons/agribusiness.png';
        }else if(industry == "retail"){
            return data_location+'Icons/retail.png';
        }else if(industry == "legal"){
            return data_location+'Icons/law.png';
        }else if(industry == "logistics"){
            return data_location+'Icons/shipping.png';
        }else if(industry == "nonprofit"){
            return data_location+'Icons/non-profit-org.png';
        }else if(industry == "realestate"){
            return data_location+'Icons/real-estate.png';
        }else if(industry == "energy"){
            return data_location+'Icons/energy.png';
        }else if(industry == "petrol"){
            return data_location+'Icons/petroleum.png';
        }else if(industry == "coordinating"){
            return data_location+'Icons/coordinating-committees.png';
        }else if(industry == "cultural"){
            return data_location+'Icons/culture.png';
        }else if(industry == "financial"){
            return data_location+'Icons/finance.png';
        }else if(industry == "aerospace"){
            return data_location+'Icons/aerospace-and-defense.png';
        }else if(industry == "pharmaceuticals"){
            return data_location+'Icons/pharma.png';
        }else if(industry == "health"){
            return data_location+'Icons/pharma.png';     
        }else if(industry == "insurance"){ 
            return data_location+'Icons/real-estate.png';
        }else if(industry == "university"){
            return data_location+'Icons/university.png';
        }else if(industry == "cultural"){ 
            return data_location+'Icons/culture.png';
        }else if(industry == "technology"){ 
            return data_location+'Icons/technology.png';
        }else if(industry == "telecom"){ 
            return data_location+'Icons/telecom.png';
        }else if(industry == "media"){ 
            return data_location+'Icons/media.png';
        }else if(industry == "manufacturing"){ 
            return data_location+'Icons/industrial-manufacturing.png';
        }else{
            return data_location+'Icons/coordinating-committees.png';
        }
    }
    
    $($(".leaflet-control-layers-overlays").children()[0]).after('<hr>');   
    $($(".leaflet-control-layers-overlays").children()[3]).after('<hr>');
    $($(".leaflet-control-layers-overlays").children()[8]).after('<hr>');
    $($(".leaflet-control-layers-overlays").children()[11]).after('<hr>');
}
