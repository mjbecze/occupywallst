function init_event_add_map(args){
    
    var chicago;
    if (args.start_point){
        chicago = new L.LatLng(args.start_point[1],args.start_point[0]);
    }else{
        chicago =  new L.LatLng(41.87, -87.62); 
    }

    var southWest = new L.LatLng(41.31668565214464,-88.80862320460763),
    northEast = new L.LatLng(42.23836288222613,-86.61135757969537,-74.125),
    bounds = new L.LatLngBounds(southWest, northEast),
    tilesetUrl =  'http://{s}.acetate.geoiq.com/tiles/acetate/{z}/{x}/{y}.png',
    tilesetAttrib = '2011 GeoIQ &#038; Stamen, Data from OSM and Natural Earth';
    
    

    var MarkerOptions = {
        draggable: function(){
	     if(args.edit){
		return true;
	     }else{
		return false;
	     }
	}
    };
    var marker = new L.Marker(chicago,MarkerOptions);
    marker.on('dragend', function(e) {
        $('#id_lat').val(e.target._latlng.lat);
        $('#id_lng').val(e.target._latlng.lng);
    });
    
    // create the tile layer with correct attribution
    var osm = new L.TileLayer(tilesetUrl, {
        minZoom: 2,
        maxZoom: 20,
        attribution: tilesetAttrib
    }); 
    // set up the map
    var map = new L.Map('map',{
        center:chicago,
        zoom: 11,
        layers: [osm]    
    });
    
    map.addLayer(marker);
    
    $("#id_location").after("<input id='geocode_button' type='button' value='Find'>");
    $("#geocode_button").click(function() {
        geocode($("#id_location").val());
    });
    
    function geocode(address){
        $.ajax({
           url:"http://www.mapquestapi.com/geocoding/v1/address?key=Fmjtd%7Cluua2q02nd%2C7n%3Do5-hr7su&boundingBox=-88.80862320460763,41.31668565214464,-86.61135757969537,42.23836288222613&location="+address,
           dataType:'jsonp',
        }).done(function(data){ 
            var latlng = data.results[0].locations[0].latLng;
            if(bounds.contains(latlng)){
                var newCenter = new L.LatLng(latlng.lat,latlng.lng);
                $('#id_lat').val(latlng.lat);
                $('#id_lng').val(latlng.lng);                
                map.setView(newCenter,13,false);
                marker.setLatLng(newCenter);
            }else{alert("Could not Find, Please drag the Marker to the location of the event");}
        });
    }
}
