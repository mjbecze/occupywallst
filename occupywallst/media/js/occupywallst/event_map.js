function init_event_map(args){
    
    var chicago =  new L.LatLng(41.87, -87.62); 
    var southWest = new L.LatLng(41.31668565214464,-88.80862320460763),
    northEast = new L.LatLng(42.23836288222613,-86.61135757969537,-74.125),
    bounds = new L.LatLngBounds(southWest, northEast),
    tilesetUrl =  'http://{s}.acetate.geoiq.com/tiles/acetate/{z}/{x}/{y}.png',
    tilesetAttrib = '2011 GeoIQ &#038; Stamen, Data from OSM and Natural Earth';
    
    // create the tile layer with correct attribution
    var osm = new L.TileLayer(tilesetUrl, {
        minZoom: 2,
        maxZoom: 20,
        attribution: tilesetAttrib
    });     
    // set up the map
    var map = new L.Map('map',{
        center:chicago,
        zoom: 11,
        layers: [osm]    
    });
    
    var ChaseMarkerOptions = {
        fillColor: "#ff7800",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
    };
    
    var eventJSON = new L.GeoJSON(null,{
            pointToLayer: function (latlng) {
                return new  L.Marker(latlng);
            }
        });

    eventJSON.on("featureparse", function (e) {
        if (e.properties && e.properties.Description){
            e.layer.bindPopup("<h1>"
            +e.properties.Name+"</h1><h2>"
            +e.properties.Start+"<br>"
            +e.properties.Description
            +" |&nbsp;<a href='"
            +e.properties.Url+"'>View&nbsp;More</a></h2>");
        }
    });
    eventJSON.addGeoJSON(args.event_json);
 
    map.addLayer(eventJSON); 
    
    var p1 = new L.LatLng(41.878285,-87.62058),
        p2 = new L.LatLng(41.878221,-87.627747),
        p3 = new L.LatLng(41.874394,-87.627581),
        p4 = new L.LatLng(41.87445,-87.62433),
        p5 = new L.LatLng(41.852765,-87.623788),
        polylinePoints = [p1, p2, p3, p4, p5],
        polyline = new L.Polyline(polylinePoints);

    polyline.bindPopup("<h2>CANG8 March</h2> Petrillo Band Shell |<a href=\"http://natoprotest.org/article/cang8-march/\">View More</a>");
    map.addLayer(polyline);
}
