r"""

    occupywallst.views
    ~~~~~~~~~~~~~~~~~~

    Dynamic web page functions.

"""
    
import logging
from hashlib import sha256
from functools import wraps
from datetime import datetime, timedelta, date
from django.db.models import Q
from django.conf import settings
from django.forms import ValidationError
from django.contrib.auth import views as authviews
from django.core.cache import cache
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import IntegrityError
#for calender
import calendar

from occupywallst import api, forms, models as db


logger = logging.getLogger(__name__)
month_abbr = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug',
              'sep', 'oct', 'nov', 'dec']
MONTHS = dict((i, s) for i, s in zip(range(1, 13), month_abbr))
iMONTHS = dict((s, i) for s, i in zip(month_abbr, range(1, 13)))


def error(request):
    assert False


def my_cache(mkkey, seconds=60):
    def _my_cache(function):
        @wraps(function)
        def __my_cache(request, *args, **kwargs):
            if request.user.is_authenticated():
                response = function(request, *args, **kwargs)
            else:
                key = sha256(mkkey(request, *args, **kwargs) + ':' +
                             request.LANGUAGE_CODE).hexdigest()
                response = cache.get(key)
                if not response:
                    response = function(request, *args, **kwargs)
                    cache.set(key, response, seconds)
            return response
        return __my_cache
    return _my_cache

@my_cache(lambda r, per_page: 'index-%d' % (per_page))
def index(request, per_page=10):
    articles = (db.Article.objects
                .filter(is_visible=True, is_forum=False, is_deleted=False, is_event=False)
                .order_by('-published'))
    articles_top = (db.Article.objects
                .select_related('carousel')
                .filter(is_visible=True, is_forum=False, is_deleted=False, is_top=True)
                .order_by('-published'))
    
    begin = datetime.now() - timedelta(days=1)            
    events = db.Event.objects.filter(is_event=True, is_deleted=False, start_date__gte=begin, ).order_by('start_date')        
    images = db.Carousel.objects.get(name="frontpage").photos.all()
    

    return render_to_response(
        'occupywallst/index.html', 
        {'articles': articles[:12],
         'articles_top':articles_top[:4],
        'events': events[:10],
        'events_list': events,
        'images': images},
        context_instance=RequestContext(request))


   

def _forum_search(query):
    from whoosh import qparser, index
    from whoosh.qparser.dateparse import DateParserPlugin
    ix = index.open_dir(settings.WHOOSH_ROOT)
    qp = qparser.QueryParser("content", ix.schema)
    qp.add_plugin(DateParserPlugin())
    with ix.searcher() as searcher:
        query = qp.parse(query)
        for result in searcher.search(query, limit=50):
            yield db.Article.objects.get(id=result['id'])


def forum_search(request):
    if not request.GET.get('q'):
        return HttpResponseRedirect('..')
    results = _forum_search(request.GET['q'])
    return render_to_response(
        'occupywallst/forum_search.html', {'results': results,
                                           'forum_search': request.GET['q']},
        context_instance=RequestContext(request))


def archive(request, is_forum, prefix, per_page,
            page=None, year=None, month=None, day=None):
    page = int(page) - 1 if page else 0
    year = int(year) if year else None
    month = iMONTHS[month[:3].lower()] if month else None
    day = int(day) if day else None
    qset = (db.Article.objects_as(request.user)
            .select_related("author", "author__userinfo")
            .filter(is_forum=is_forum)
            .order_by('-published'))
    if year and month:
        filterday = datetime(year, month, day or 1)
        qset = qset.filter(published__year=year, published__month=month)
        smonth = MONTHS[month].capitalize()
        drill = qset.dates('published', 'day')
        if day:
            mode = 'day'
            qset = qset.filter(published__day=day)
            path = '%s%s-%d-%d/' % (prefix, smonth, day, year)
        else:
            mode = 'month'
            path = '%s%s-%d/' % (prefix, smonth, year)
    else:
        mode = 'all'
        drill = qset.dates('published', 'month')
        filterday = None
        path = prefix
    articles = qset[page * per_page:page * per_page + per_page]
    fool = (len(articles) == per_page)  # 90% correct without count(*)
    return render_to_response(
        'occupywallst/archive.html', {
            'articles': articles,
            'is_forum': is_forum,
            'prefix': prefix,
            'mode': mode,
            'drill': drill,
            'filterday': filterday,
            'prev_path': '/%spage-%d/' % (path, page + 0) if page else None,
            'cano_path': '/%spage-%d/' % (path, page + 1),
            'next_path': '/%spage-%d/' % (path, page + 2) if fool else None},
        context_instance=RequestContext(request))


@my_cache(lambda r: 'forum')
def forum(request):
    per_page = 25
    articles = (db.Article.objects_as(request.user)
                .select_related("author", "author__userinfo")
                .order_by('-killed'))
    bests = cache.get('forum:bests')
    if not bests:
        bests = (db.Comment.objects
                 .select_related("article", "user", "user__userinfo")
                 .filter(is_removed=False, is_deleted=False)
                 .filter(published__gt=datetime.now() - timedelta(days=1))
                 .order_by('-karma'))[:7]
        cache.set('forum:bests', bests, 60 * 15)
    recents = (db.Comment.objects_as(request.user)
               .select_related("article", "user", "user__userinfo")
               .order_by('-published'))[:20]
    return render_to_response(
        'occupywallst/forum.html', {'articles': articles[:per_page],
                                    'bests': bests,
                                    'recents': recents,
                                    'per_page': 50},
        context_instance=RequestContext(request))


@my_cache(lambda r: 'forum_comments')
def forum_comments(request):
    per_page = 25
    comments = (db.Comment.objects_as(request.user)
                .select_related("article", "user", "user__userinfo")
                .order_by('-published'))
    return render_to_response(
        'occupywallst/forum_comments.html', {'comments': comments[:per_page]},
        context_instance=RequestContext(request))


def bonus(request, page):
    """Render page based on Verbiage table entry"""
    try:
        res = db.Verbiage.get('/' + page, request.LANGUAGE_CODE)
    except ObjectDoesNotExist:
        raise Http404()
    if hasattr(res, 'render'):
        res = res.render(RequestContext(request))
    return HttpResponse(res)


def _instate_hierarchy(comments):
    """Rearranges list of comments into hierarchical structure

    This adds the pseudo-field "replies" to each comment.
    """
    for com in comments:
        com.replies = []
    comhash = dict([(c.id, c) for c in comments])
    res = []
    for com in comments:
        if com.parent_id is None:
            res.append(com)
        else:
            if com.parent_id in comhash:
                comhash[com.parent_id].replies.append(com)
    return res


@my_cache(lambda r, slug, forum=False:
              ('artfrm:' if forum else 'artnwz:') + slug)
def article(request, slug, forum=False):
    try:
        article = (db.Article.objects
                   .select_related("author","event")
                   .get(slug=slug, is_deleted=False))
        if not forum and article.is_forum:
            raise db.Article.DoesNotExist()
    except db.Article.DoesNotExist:
        raise Http404()
    def get_comments():
        comments = article.comments_as_user(request.user)
        comments = _instate_hierarchy(comments)
        for comment in comments:
            yield comment
    recents = (db.Article.objects
               .select_related("author")
               .filter(is_visible=True, is_deleted=False)
               .order_by('-published'))
    if not forum:
        recents = recents.filter(is_forum=False)
    return render_to_response(
        "occupywallst/article.html", {'article': article,
                                      'comments': get_comments(),
                                      'recents': recents[:25],
                                      'forum': forum},
        context_instance=RequestContext(request))



def thread(request, slug):
    return article(request, slug, forum=True)

def user_page(request, username):
    try:
        user = (db.User.objects
                .filter(is_active=True)
                .select_related("userinfo")
                .get(username=username))
    except db.User.DoesNotExist:
        raise Http404()
    if user.userinfo.position is not None:
        nearby_users = (db.UserInfo.objects
                        .select_related("user")
                        .filter(position__isnull=False)
                        .distance(user.userinfo.position)
                        .order_by('distance'))[1:10]
    else:
        nearby_users = []
    if request.user.is_authenticated():
        messages = (db.Message.objects
                    .select_related("from_user", "from_user__userinfo",
                                    "to_user", "to_user__userinfo")
                    .filter(Q(from_user=user, to_user=request.user) |
                            Q(from_user=request.user, to_user=user))
                    .filter(is_deleted=False)
                    .order_by('-published'))
        for message in messages:
            if message.to_user == request.user and message.is_read == False:
                message.is_read = True
                message.save()
    else:
        messages = []
    return render_to_response(
        'occupywallst/user.html', {'obj': user,
                                   'messages': messages,
                                   'nearby': nearby_users},
        context_instance=RequestContext(request))


@login_required
def notification(request, id):
    try:
        notify = db.Notification.objects.get(id=id, user=request.user)
    except db.Notification.DoesNotExist:
        raise Http404()
    if not notify.is_read:
        notify.is_read = True
        notify.save()
    return HttpResponseRedirect(notify.url)


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(request.user.get_absolute_url())
    return authviews.login(
        request, template_name="occupywallst/login.html")


def logout(request):
    return authviews.logout(
        request, template_name="occupywallst/logged_out.html")


def signup(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(request.user.get_absolute_url())
    if request.method == 'POST':
        form = forms.SignupForm(request.POST)
        if form.is_valid():
            key = 'signup_' + request.META['REMOTE_ADDR']
            #if cache.get(key):
            #    return HttpResponse('please wait before signing up again')
            cache.set(key, True, settings.OWS_LIMIT_SIGNUP)
            form.save()
            api.login(request, form.cleaned_data.get('username'),
                      form.cleaned_data.get('password'))
            url = request.user.get_absolute_url()
            return HttpResponseRedirect(url + '?new=1')
    else:
        form = forms.SignupForm()
    return render_to_response(
        'occupywallst/signup.html', {'form': form},
        context_instance=RequestContext(request))


@login_required
def edit_profile(request, username):
    if username != request.user.username:
        url = request.user.get_absolute_url()
        return HttpResponseRedirect(url + 'edit/')
    if request.method == 'POST':
        form = forms.ProfileForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.user.get_absolute_url())
    else:
        form = forms.ProfileForm(request.user)
    return render_to_response(
        'occupywallst/edit_profile.html', {'form': form},
        context_instance=RequestContext(request))
        

##calender code
def named_month(month_number):
    """
    Return the name of the month, given the number.
    """
    return date(1900, month_number, 1).strftime("%B")

def this_month(request):
    """
    Show calendar of events this month.
    """
    today = datetime.now()
    return calendar_render(request, today.year, today.month)


def calendar_render(request, year, month, series_id=None):
    """
    Show calendar of events for a given month of a given year.
    ``series_id``
    The event series to show. None shows all event series.

    """

    my_year = int(year)
    my_month = int(month)
    my_calendar_from_month = datetime(my_year, my_month, 1)
    my_calendar_to_month = datetime(my_year, my_month,calendar.monthrange(my_year, my_month)[1])

    my_events = db.Event.objects.filter(is_deleted=False).filter(start_date__gte=my_calendar_from_month).filter(start_date__lte=my_calendar_to_month).order_by('start_date')

    #if series_id:
    #my_events = my_events.filter(series=series_id)

    # Calculate values for the calendar controls. 1-indexed (Jan = 1)
    my_previous_year = my_year
    my_previous_month = my_month - 1
    if my_previous_month == 0:
        my_previous_year = my_year - 1
        my_previous_month = 12
    my_next_year = my_year
    my_next_month = my_month + 1
    if my_next_month == 13:
        my_next_year = my_year + 1
        my_next_month = 1
    my_year_after_this = my_year + 1
    my_year_before_this = my_year - 1
    return render_to_response("occupywallst/cal_template.html", { 'events_list': my_events,
                                                        'month': my_month,
                                                        'month_name': named_month(my_month),
                                                        'year': my_year,
                                                        'previous_month': my_previous_month,
                                                        'previous_month_name': named_month(my_previous_month),
                                                        'previous_year': my_previous_year,
                                                        'next_month': my_next_month,
                                                        'next_month_name': named_month(my_next_month),
                                                        'next_year': my_next_year,
                                                        'year_before_this': my_year_before_this,
                                                        'year_after_this': my_year_after_this,
    }, context_instance=RequestContext(request))

@my_cache(lambda r: 'events')
def events(request):

    begin = datetime.now() - timedelta(days=1)

    if request.method == 'POST' and "end" in request.POST and request.POST["end"] != "":
        if "begin" in request.POST and request.POST['begin'] != "": 
            begin = request.POST['begin']
        my_events = db.Event.objects.filter(is_event=True, is_deleted=False, start_date__gte=begin).filter(start_date__lte=request.POST['end']).order_by('-start_date')
    else:
        my_events = db.Event.objects.filter(is_event=True, is_deleted=False, start_date__gte=begin, ).order_by('start_date')       
    
    return render_to_response(
        'occupywallst/events.html', {'events_list': my_events},
        context_instance=RequestContext(request))

@login_required
def edit_event(request,id):
    event = get_object_or_404(db.Event, pk=int(id))
    if event != request.user.username:
        return add_event(request, event)   
 
@login_required
def add_event(request,instance=None):
    #my_events = db.Event.objects.filter(date__gte=datetime.now())
    if request.method == 'POST':
        form = forms.EventForm(request.POST,instance=instance)
        if form.is_valid():
            event = form.save(request.user)
            return HttpResponseRedirect(event.get_absolute_url())
    else:
        form = forms.EventForm(instance=instance)
        
    return render_to_response(
        'occupywallst/event_form.html', {'form': form,
                                          'event':instance},
        context_instance=RequestContext(request))

def images(request):
    carousels = db.Carousel.objects.select_related('photos').filter(is_article=False)
    return render_to_response(
        'occupywallst/images.html', {'carousels': carousels},
        context_instance=RequestContext(request))

def map(request,category=None):
    begin = datetime.now() - timedelta(days=1)
    my_events = db.Event.objects.filter(is_event=True, is_deleted=False, start_date__gte=begin, ).order_by('start_date')       
    return render_to_response(
        'occupywallst/map.html', {'events_list': my_events,
				   'category':category},
        context_instance=RequestContext(request))
    
