var maps_init;

(function() {
    "use strict";
    var geocoder;
    var infowindow;
    var address;
    var marker;
    
    function geocodePosition(pos) {
      geocoder.geocode({
        latLng: pos
      }, function(responses) {
        if (responses && responses.length > 0) {
          
           updateMarkerAddress(responses[0].formatted_address);
            
        } else {
          updateMarkerAddress('Cannot determine address at this location.');
        }
      });
    }

    function updateMarkerPosition(latLng) {
        $('#id_rendezvous_lat').val(latLng.lat());
        $('#id_rendezvous_lng').val(latLng.lng());
    }

    function updateMarkerAddress(str) {
        infowindow.setContent('<b>Approximant Address</b><br>'+str+'');
        $('#id_rendezvous_address').val(str);
        address = str;
    }

    function initialize(args) {
        
        geocoder = new google.maps.Geocoder();
        var latLng = new google.maps.LatLng(-34.397, 150.644);
        var map = new google.maps.Map(args.map, {
            zoom: args.zoom,
            center: args.center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    
         //create route
        if (args.initial_polyline) {
            var bounds = new google.maps.LatLngBounds();
            //does this really work?
            var polyline_arr = args.initial_polyline.map(function(e) {
                var latlng = new google.maps.LatLng(e[0],e[1]);
                bounds.extend(latlng);
                return latlng;
            })
            
            var polyline = happy_line(polyline_arr);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        }

        //create rendezvous marker
        if (args.rendezvous) {
            var len = args.rendezvous.length;
            var rmarker;
            var position; 
            var rWindow = new google.maps.InfoWindow();
            while (len--){
                position = new google.maps.LatLng(args.rendezvous[len][0],args.rendezvous[len][1]);
                rmarker = new google.maps.Marker({
                    position: position,
                    title: 'rendezvous Point',
                    map: map,
                    draggable: false
                });
                
                google.maps.event.addListener(rmarker, 'click', (function(rmarker, len) {
                    return function() {
                      rWindow.setContent("<b>"+args.rendezvous[len][3]+"</b><br>approximate rendezvous<br> "+args.rendezvous[len][2]);
                      rWindow.open(map, rmarker);
                    }
                  })(rmarker, len));
                  
            }
        }   
            //create marker
        if (typeof args.marker  != 'undefined'){
                var position = (args.marker ? pos_to_latlng(args.marker) : map.getCenter());
                marker = new google.maps.Marker({
                position: position,
                title: 'Pick up Point',
                map: map,
                draggable: args.draggable
            });
            
            //create info window
            infowindow = new google.maps.InfoWindow();
            infowindow.setContent(args.address || "Please Select a Rendezvous");
            infowindow.open(map, marker);
            
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });
            
            // Update current position info.
            updateMarkerPosition(latLng);
          
            // Add dragging event listeners.
            google.maps.event.addListener(marker, 'dragstart', function() {
                updateMarkerAddress("<img src='/media/img/ajax-loader-32.gif'>");
                
                
            });
          
            google.maps.event.addListener(marker, 'drag', function() {
                
                
            });
          
            google.maps.event.addListener(marker, 'dragend', function() {
                updateMarkerPosition(marker.getPosition());
                geocodePosition(marker.getPosition());
                $('input[type="submit"]').removeAttr('disabled');
                //$('button').removeAttr('style');
            });
        }

        function happy_line(path) {
            var line = new google.maps.Polyline({
                map: map,
                path: path,
                strokeOpacity: 0.4,
                strokeWeight: 5,
                strokeColor: 'blue',
                zIndex: 1
            });
            google.maps.event.addListener(line, 'mouseover', function(e) {
                line.setOptions({
                    strokeColor: 'red',
                    strokeOpacity: 0.7,
                    strokeWeight: 10,
                    zIndex: 10
                });
            });
            google.maps.event.addListener(line, 'mouseout', function(e) {
                line.setOptions({
                    strokeColor: 'blue',
                    strokeOpacity: 0.4,
                    strokeWeight: 5,
                    zIndex: 1
                });
            });
            return line;
        }

        function pos_to_latlng(position) {
            return new google.maps.LatLng(position[0], position[1]);
        }
      

    }
    
/*  
    $("#cancel_ride").click(function(){
        var pathname = window.location.pathname;
        var patt=/\d+/;
         
        alert(pathname.match(patt)[0]);
        api("/api/ride_request_delete/", {
            "request_id":pathname.match(patt)[0]
        }, function(data) {
            
        })

    });
    

    $('form').submit(function(){
        // Maybe show a loading indicator...
        $.post(this.action, $(this).serialize(), function(res){
            // Do something with the response `res`
            console.log(res);
            $("#edit_rendezvous").hide();
            // Don't forget to hide the loading indicator!
        });
        return false; // prevent default action
    });
    */
    /*
    $("#edit_rendezvous").click(function(){
        var pathname = window.location.pathname;
        var patt=/\d+/;
         
        var pos = marker.getPosition();
        api("/api/ride_edit_rendezvous/",  $('form').serialize(), function(data) {
            $("#edit_rendezvous").hide();
        })

    });
    */

    maps_init = initialize;
})();
